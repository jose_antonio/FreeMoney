class ProjectController < ApplicationController
  getter project = Project.new

  before_action do
    only [:show, :edit, :update, :destroy] { set_project }
  end

  def index
    projects = Project.all
    render "index.slang"
  end

  def show
    render "show.slang"
  end

  def new
    render "new.slang"
  end

  def edit
    render "edit.slang"
  end

  def update
    project.set_attributes project_params.validate!
    if project.save
      redirect_to action: :index, flash: {"success" => "Updated project successfully."}
    else
      flash["danger"] = "Could not update Project!"
      render "edit.slang"
    end
  end

  def create
    project = Project.new project_params.validate!
    if project.save
      redirect_to action: :index, flash: {"success" => "Created project successfully."}
    else
      flash["danger"] = "Could not create Project!"
      render "new.slang"
    end
  end

  def destroy
    project.destroy
    redirect_to action: :index, flash: {"success" => "Deleted project successfully."}
  end

  private def project_params
    params.validation do
      required :uuid { |f| !f.nil? }
      required :maintainer { |f| !f.nil? }
      required :repository { |f| !f.nil? }
    end
  end

  private def set_project
    @project = Project.find! params[:id]
  end
end
