require "granite/adapter/mysql"
require "crypto/bcrypt/password"
require "uuid"

class User < Granite::Base
  include Crypto
  adapter mysql
  primary uuid : String, auto: false
  field nickname : String
  field hashed_password : String
  timestamps

  before_create :assign_uuid

  validate :nickname, "is required", ->(user : User) do
    (nickname = user.nickname) ? !nickname.empty? : false
  end

  validate :password, "is too short", ->(user : User) do
    user.password_changed? ? user.valid_password_size? : true
  end

  def password=(password)
    @new_password = password
    @hashed_password = Bcrypt::Password.create(password, cost: 10).to_s
  end

  def password
    (hash = hashed_password) ? Bcrypt::Password.new(hash) : nil
  end

  def password_changed?
    new_password ? true : false
  end

  def valid_password_size?
    (pass = new_password) ? pass.size >= 8 : false
  end

  def authenticate(password : String)
    (bcrypt_pass = self.password) ? bcrypt_pass == password : false
  end

  def assign_uuid
    @uuid = UUID.random.to_s
  end

  private getter new_password : String?
end
