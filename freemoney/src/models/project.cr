require "granite/adapter/mysql"
require "uuid"

class Project < Granite::Base
  adapter mysql
  table_name projects

  # id : Int64 primary key is created for you
  field uuid : String
  field maintainer : String
  field repository : String
  timestamps
end
