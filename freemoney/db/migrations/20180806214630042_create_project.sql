-- +micrate Up
CREATE TABLE projects (
  id INTEGER,
  uuid VARCHAR(50) PRIMARY KEY,
  maintainer VARCHAR(50),
  repository VARCHAR(50),
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);


-- +micrate Down
DROP TABLE IF EXISTS projects;
