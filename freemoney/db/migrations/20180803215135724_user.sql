-- +micrate Up
CREATE TABLE users (
  uuid VARCHAR(50) PRIMARY KEY,
  nickname VARCHAR(50),
  hashed_password VARCHAR(100),
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);


-- +micrate Down
DROP TABLE IF EXISTS users;
