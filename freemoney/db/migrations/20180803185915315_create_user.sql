-- +micrate Up
CREATE TABLE users (
  id BIGINT PRIMARY KEY,
  email VARCHAR(50),
  hashed_password VARCHAR(100),
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);


-- +micrate Down
DROP TABLE users;
