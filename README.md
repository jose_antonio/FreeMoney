# FreeMoney

Platform for monetization of free software development

## Config 

### Requirements

This app requires the following to be pre-installed:

* Amber (submodule included with app)
* MySql (see for setup below)
* NodeJS

### Database Setup

The setup uses the MySQL database. The default settings are as follows:

* username: freemoney
* password: betterbadger

These can be modified in `{project_name}/config/environments/{environment}.yml` in the `database_url` parameter. The user requires to have all permissions to create, modify, and access the databases.
